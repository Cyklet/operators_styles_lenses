import UIKit

final class ViewController: UIViewController {
    private let tests = StressTests()
    private let type: IncrementType = .mutable

    @IBAction func startStress(_ sender: Any) {
        tests.start(type: type)
    }

    @IBAction func print(_ sender: Any) {
        tests.printMe(type: type)
    }
}

