import UIKit

func styleSearchBar(tintColor: UIColor? = nil, barTintColor: UIColor? = nil) -> (UISearchBar) -> Void {
	return { bar in
		bar.tintColor = tintColor
		bar.barTintColor = barTintColor
	}
}

