import UIKit

var defaultBarStyle: (UINavigationBar) -> Void {
	return styleNavigationBar(tintColor: .black, barTintColor: .white, titleColor: .black)
}

func styleNavigationBar(style: UIBarStyle? = nil, tintColor: UIColor? = nil,
						barTintColor: UIColor? = nil, titleColor: UIColor? = nil) -> (UINavigationBar) -> Void {
	return { bar in
		if let style = style { bar.barStyle = style }
		if let tintColor = tintColor { bar.tintColor = tintColor }
		if let barTintColor = barTintColor { bar.barTintColor = barTintColor }
		if let titleColor = titleColor { bar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: titleColor] }
	}
}

