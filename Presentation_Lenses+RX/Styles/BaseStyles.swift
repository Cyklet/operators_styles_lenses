import UIKit

func styleRounded(radius: CGFloat) -> (UIView) -> Void {
	return { view in
		view.layer.cornerRadius = radius
		view.layer.masksToBounds = true
	}
}

func styleBordered(color: UIColor, width: CGFloat) -> (UIView) -> Void {
	return { view in
		view.layer.borderColor = color.cgColor
		view.layer.borderWidth = width
	}
}

func styleShadowed(color: UIColor, offset: CGSize) -> (UIView) -> Void {
	return { view in
		view.layer.shadowColor = color.cgColor
		view.layer.shadowOffset = offset
	}
}

func styleButtonTitled(font: UIFont, titleColor: UIColor) -> (UIButton) -> Void {
	return { button in
		button.titleLabel?.font = font
		button.setTitleColor(titleColor, for: .normal)
	}
}

private final class StyleExamples {
	private let view = UIView()
	private let button = UIButton()

	private func withoutCustomOperators() {
		styleRounded(radius: 10)(view)
		styleBordered(color: .black, width: 10)(view)
		styleShadowed(color: .black, offset: .zero)(view)
	}

	private func withCustomFunction() {
		let defaultViewStyle = concat(styleRounded(radius: 10),
									  styleBordered(color: .black, width: 10),
									  styleShadowed(color: .black, offset: .zero))
		defaultViewStyle(view)
	}

	private func customOperators() {
		// (UIView) -> Void
		let defaultViewStyle =
			styleRounded(radius: 10)
			<> styleBordered(color: .black, width: 10)
			<> styleShadowed(color: .black, offset: .zero)

		defaultViewStyle(view)

		// With operator ->

		view |> defaultViewStyle

		// OR

		view
			|> styleRounded(radius: 10)
			<> styleBordered(color: .black, width: 10)
			<> styleShadowed(color: .black, offset: .zero)
			<> { $0.backgroundColor = .red }
			<> { $0.alpha = 0.6 } // etc.

		// BUTTON

		button
			|> styleRounded(radius: 10)
			<> styleBordered(color: .black, width: 10)
			<> styleShadowed(color: .black, offset: .zero)
			<> styleButtonTitled(font: .boldSystemFont(ofSize: 10), titleColor: .white)
			<> { $0.backgroundColor = .red }
			<> { $0.alpha = 0.6 } // etc.
	}
}


func concat<A: AnyObject>(_ sequence: (A) -> Void...) -> (A) -> Void {
	return { view in
		sequence.forEach { $0(view) }
	}
}
