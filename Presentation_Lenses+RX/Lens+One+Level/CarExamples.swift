enum IncrementType: String {
    case all
    case imutable
    case lens
    case mutable
    case mutableClass
}

// https://broomburgo.github.io/fun-ios/post/lenses-and-prisms-in-swift-a-pragmatic-approach/

// http://narf.pl/posts/lenses-in-swift

final class CarExamples {
    private static let defaultName = "Audi"
    private static let defaultMileage: Double = 0
    var imutableCar = CarImutable(name: defaultName, mileage: defaultMileage)   // 2 or more inits in the autocomplete
    var lensCar = CarLens(name: defaultName, mileage: defaultMileage)
	// Struct - Value Type, When you copy a value type (i.e., when it’s assigned, initialized or passed into a function), each instance keeps a unique copy of the data. If you change one instance, the other doesn’t change too.
	var mutableCar = CarMutable(name: defaultName, mileage: defaultMileage)
	// Class - Reference Type, When you copy a referenve type, each instance shares the data. The reference itself is copied, but not the data it references. When you change one, the other changes too.
	let mutableClassCar = CarClass(name: defaultName, mileage: defaultMileage)


    func incremet(value: Double, type: IncrementType = .all) {
        switch type {
        case .all:
            incrementMutable(value: value)
            incrementMutableClass(value: value)
            incrementImutable(value: value)
            incrementLens(value: value)
        case .imutable:
            incrementImutable(value: value)
        case .lens:
            incrementLens(value: value)
        case .mutable:
            incrementMutable(value: value)
        case .mutableClass:
            incrementMutableClass(value: value)
        }
    }

    func printMileage(type: IncrementType) {
        switch type {
        case .all:
            print("LENS : \(lensCar.mileage) IMUTABLE : \(imutableCar.mileage) MUTABLE : \(mutableCar.mileage) MUTABLECLASS : \(mutableClassCar.mileage)")
        case .lens:
            print(type.rawValue.uppercased() + " : " + String(lensCar.mileage))
        case .imutable:
            print(type.rawValue.uppercased() + " : " + String(imutableCar.mileage))
        case .mutable:
            print(type.rawValue.uppercased() + " : " + String(mutableCar.mileage))
        case .mutableClass:
            print(type.rawValue.uppercased() + " : " + String(mutableClassCar.mileage))
        }
    }

    // MARK: - Private functions

    // MARK: One Level

    private func incrementImutable(value: Double) {
        let newValue = imutableCar.mileage + value
        _ = CarImutable(car: imutableCar, mileage: newValue)
    }

    private func incrementLens(value: Double) {
        let newValue = lensCar.mileage + value

        // Without operators
        _ = CarLens.Lenses.mileage.set(newValue, lensCar)

        // With lens set operator
        _ = (CarLens.Lenses.mileage .~ newValue)(lensCar)

        // With lens set operator + pipe operator
        _ =  lensCar |> CarLens.Lenses.mileage .~ newValue
    }

    //  In the mutliple levels structure we are forced to make the whole hierarchy var in case that one parameter
    //  on the last level is mutable, eventualy we will end up with the whole struct/class in a mutable state.

    private func incrementMutable(value: Double) {
        mutableCar.mileage += value
    }

    private func incrementMutableClass(value: Double) {
        mutableClassCar.mileage += value
    }
}
