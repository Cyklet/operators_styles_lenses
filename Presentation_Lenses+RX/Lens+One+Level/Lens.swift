struct Lens<Whole, Part> {
    let get: (Whole) -> Part
    let set: (Part, Whole) -> Whole

    func compose<Subpart>(_ rhs: Lens<Part, Subpart>) -> Lens<Whole, Subpart> {
        return Lens<Whole, Subpart>(
            get: self.get >>> rhs.get,
            set: { subPart, whole in
                let part = self.get(whole)
                let newPart = rhs.set(subPart, part)
                return self.set(newPart, whole)
        })
    }
}

