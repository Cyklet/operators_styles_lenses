struct CarMutable {
    let name: String
    var mileage: Double

    init(name: String, mileage: Double) {
        self.name = name
        self.mileage = mileage
    }
}
