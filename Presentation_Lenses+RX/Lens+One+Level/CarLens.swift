struct CarLens {
    let name: String
    let mileage: Double

    enum Lenses {
        static let mileage = Lens<CarLens, Double>(
            get: { $0.mileage },
            set: { CarLens(name: $1.name, mileage: $0) }
        )

        static let name = Lens<CarLens, String>(
            get: { $0.name },
            set: { CarLens(name: $0, mileage: $1.mileage) }
        )
    }
}
