import Foundation

final class StressTests {
	let carExamples = CarExamples()
    private var currentValue: Double = 0

    func start(type: IncrementType) {
        let firstQueue = DispatchQueue(label: "first")
        let secondQueue = DispatchQueue(label: "second")
        let thirdQueue = DispatchQueue(label: "third")

        increment(type: type, queue: firstQueue)
        increment(type: type, queue: secondQueue)
        increment(type: type, queue: thirdQueue)

        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.printMe(type: type)
        }
    }

    func printMe(type: IncrementType) {
        carExamples.printMileage(type: type)
        print("Current Value: \(currentValue)")
    }

    private func increment(type: IncrementType, queue: DispatchQueue) {
        let cycles = 100
        for index in 0...cycles {
            currentValue += Double(index)
            queue.async {
                self.carExamples.incremet(value: Double(index))
            }
        }
    }

}
