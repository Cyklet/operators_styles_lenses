struct CarImutable {
    let name: String
    let mileage: Double

    init(name: String, mileage: Double) {
        self.name = name
        self.mileage = mileage
    }

    init(car: CarImutable, mileage: Double) {
        self.name = car.name
        self.mileage = mileage
    }
}
