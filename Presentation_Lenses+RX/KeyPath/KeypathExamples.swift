final private class KeypathExamples {
	private var user: User

	init() {
		let address = Address(street: "stadel", city: "zurich")
		let contact = Contact(phone: "unknown", mail: "leshop", address: address)
		let ammount = Ammount(value: 10, currency: "chf", id: "chf1001", date: "now", state: "unknown")
		let salary = Salary(ammount: ammount, bonus: 10, feet: 10, idm: 10, dcv: 10, weer: 10, sds: 10)
		user = User(fName: "Leshop", lName: "App", contact: contact, salary: salary)
	}

//	Key paths come in three main variants:
//	KeyPath: Provides read-only access to a property.
//	WritableKeyPath: Provides readwrite access to a mutable property with value semantics (so the instance in question also needs to be mutable for writes to be allowed).
//	ReferenceWritableKeyPath: Can only be used with reference types (such as instances of a class), and provides readwrite access to any mutable property.
	func keyPath() {
		// Getter way binded to the property
		_ = user.salary.ammount.currency
		// KeyPath is stateless
		let keyPath = \User.salary.ammount.currency
		_ = user[keyPath: keyPath]


		// High order functions
		let doppelgangers = [user, user, user]

		_ = doppelgangers.map { $0.fName }
		_ = doppelgangers.map { $0[keyPath: \.fName] }
		_ = doppelgangers.map(get(\.fName))
		_ = doppelgangers.map(^\.fName)
		_ = doppelgangers.map(^\.salary.ammount.value)

		let salaryBonus = (^\User.salary.bonus >>> Int.init >>> String.init)
		_ = doppelgangers.map(salaryBonus) // [String]
		_ = doppelgangers.filter(^\.isOld)
		_ = doppelgangers.filter(^\.isOld >>> (!))
		_ = doppelgangers.sorted(by: { $0.fName < $1.fName }) // (User, User) throws -> Bool
		_ = doppelgangers
			.sorted(by: their(^\.fName, <))
			.sorted(by: their(^\.lName, >))

		_ = prop(\User.fName)  // ((String) -> String) -> (User) -> User
		_ = (prop(\User.fName)) { $0.uppercased() }  // (User) -> User
		_ = prop(\User.salary) <<< prop(\Salary.ammount) <<< prop(\Ammount.date) // ((String) -> String) -> (User) -> User
		_ = prop(\Ammount.date) >>> prop(\Salary.ammount) >>> prop(\User.salary) // ((String) -> String) -> (User) -> User

		_ = user
			|> (prop(\User.fName)) { $0.uppercased() }
			|> (prop(\User.salary.ammount.state)) { _ in return "Paid" }

		_ = (prop(\User.salary) <<< prop(\.ammount) <<< prop(\.value)) { $0 * 100 }
		let richer = (prop(\User.salary.ammount.value)) { $0 * 100 }
		let passportChange = (prop(\User.fName)) { _ in return "Anon"} <<< (prop(\User.lName)) { _ in return "Anon"}
		let newPattern = richer <> passportChange
		_ = user |> newPattern

		let newUser = createNewUser(from: user, newPattern)
		print(user, " vs ", newUser)
	}

	func createNewUser(from user: User, _ f: (User) -> User) -> User {
		return user |> f
	}
}


func prop<Root, Value>(_ kp: WritableKeyPath<Root, Value>) -> (@escaping (Value) -> Value) -> (Root) -> Root {
	return { update in
		{ root in
			var copy = root
			copy[keyPath: kp] = update(copy[keyPath: kp])
			return copy
		}
	}
}

func get<Root, Value>(_ kp: WritableKeyPath<Root, Value>) -> (Root) -> Value {
	return { root in
		root[keyPath: kp]
	}
}

prefix operator ^

prefix func ^<Root, Value>(_ kp: WritableKeyPath<Root, Value>) -> (Root) -> Value {
	return get(kp)
}


func their<Root, Value>(_ f: @escaping (Root) -> Value, _ g: @escaping (Value, Value) -> Bool) -> (Root, Root) -> Bool {
	return { g(f($0), f($1)) }
}
