struct InspectionItem: Codable {
    let id: String
    let type: String
    let order: Int
    let titleText: String?
    let placeholderText: String?
    let tooltipText: String?
    let stateTypes: [String]
    let decorators: [Double]
    let options: [String]
    let tag: Int?
    let image: String?
}

extension InspectionItem {
    init(id: String, type: String, order: Int, inspectionItem: InspectionItem) {
        self.id = id
        self.type = type
        self.order = order
        self.titleText = inspectionItem.titleText
        self.placeholderText = inspectionItem.placeholderText
        self.tooltipText = inspectionItem.tooltipText
        self.stateTypes = inspectionItem.stateTypes
        self.decorators = inspectionItem.decorators
        self.options = inspectionItem.options
        self.tag = inspectionItem.tag
        self.image = inspectionItem.image
    }

    init(id: String, inspectionItem: InspectionItem) {
        self.id = id
        self.type = inspectionItem.type
        self.order = inspectionItem.order
        self.titleText = inspectionItem.titleText
        self.placeholderText = inspectionItem.placeholderText
        self.tooltipText = inspectionItem.tooltipText
        self.stateTypes = inspectionItem.stateTypes
        self.decorators = inspectionItem.decorators
        self.options = inspectionItem.options
        self.tag = inspectionItem.tag
        self.image = inspectionItem.image
    }

    init(type: String, order: Int, inspectionItem: InspectionItem) {
        self.id = inspectionItem.id
        self.type = type
        self.order = order
        self.titleText = inspectionItem.titleText
        self.placeholderText = inspectionItem.placeholderText
        self.tooltipText = inspectionItem.tooltipText
        self.stateTypes = inspectionItem.stateTypes
        self.decorators = inspectionItem.decorators
        self.options = inspectionItem.options
        self.tag = inspectionItem.tag
        self.image = inspectionItem.image
    }

    // Update current item type
    init(item: InspectionItem, type: String) {
        id = item.id
        self.type = type
        order = item.order
        titleText = item.titleText
        tooltipText = item.tooltipText
        stateTypes = item.stateTypes
        decorators = item.decorators
        options = item.options
        placeholderText = item.placeholderText
        tag = item.tag
        image = item.image
    }

    // Update current item decorators
    init(item: InspectionItem, updatedDecorators: [Double], type: String) {
        id = item.id
        self.type = type
        order = item.order
        titleText = item.titleText
        tooltipText = item.tooltipText
        stateTypes = item.stateTypes
        decorators = updatedDecorators
        options = item.options
        placeholderText = item.placeholderText
        tag = item.tag
        image = item.image
    }


    enum Lenses {
        static let decorators = Lens<InspectionItem, [Double]>(
            get: { $0.decorators },
            set: { InspectionItem(id: $1.id, type: $1.type, order: $1.order,
                                  titleText: $1.titleText, placeholderText: $1.placeholderText, tooltipText: $1.tooltipText,
                                  stateTypes: $1.stateTypes, decorators: $0, options: $1.options,
                                  tag: $1.tag, image: $1.image) }
        )

        static let type = Lens<InspectionItem, String>(
            get: { $0.type },
            set: { InspectionItem(id: $1.id, type: $0, order: $1.order,
                                  titleText: $1.titleText, placeholderText: $1.placeholderText, tooltipText: $1.tooltipText,
                                  stateTypes: $1.stateTypes, decorators: $1.decorators, options: $1.options,
                                  tag: $1.tag, image: $1.image) }
        )

        static let order = Lens<InspectionItem, Int>(
            get: { $0.order },
            set: { InspectionItem(id: $1.id, type: $1.type, order: $0,
                                  titleText: $1.titleText, placeholderText: $1.placeholderText, tooltipText: $1.tooltipText,
                                  stateTypes: $1.stateTypes, decorators: $1.decorators, options: $1.options,
                                  tag: $1.tag, image: $1.image) }
        )

        static let id = Lens<InspectionItem, String>(
            get: { $0.id },
            set: { InspectionItem(id: $0, type: $1.type, order: $1.order,
                                  titleText: $1.titleText, placeholderText: $1.placeholderText, tooltipText: $1.tooltipText,
                                  stateTypes: $1.stateTypes, decorators: $1.decorators, options: $1.options,
                                  tag: $1.tag, image: $1.image) }
        )
    }
}

final class RealExample {
    func example() {
        let dummyItem = InspectionItem(id: "", type: "", order: 0, titleText: "", placeholderText: "",
                                       tooltipText: "", stateTypes: [], decorators: [], options: [], tag: 0,
                                       image: "")
        var initItem = dummyItem

        // INITs

        initItem = InspectionItem(item: initItem, updatedDecorators: [0, 1, 2], type: "Additional")
        initItem = InspectionItem(item: initItem, type: "Mega type")
        initItem = InspectionItem(type: "Ordered", order: 2, inspectionItem: initItem)
        initItem = InspectionItem(id: "New ID", inspectionItem: initItem)
        initItem = InspectionItem(id: "1", type: "Newest", order: 10, inspectionItem: initItem)


        var lensNoOperators = dummyItem
        // Lenses - without Operators

        lensNoOperators = InspectionItem.Lenses.decorators.set([0, 1, 2], lensNoOperators)
        lensNoOperators = InspectionItem.Lenses.type.set("Additional", lensNoOperators)
        lensNoOperators = InspectionItem.Lenses.type.set("Mega type", lensNoOperators)
        /// Operators will help us, they are good and beatufiul don't blame smth yet that you can't read it's just a begining
        lensNoOperators = InspectionItem.Lenses.type.set("Ordered", lensNoOperators)
        lensNoOperators = InspectionItem.Lenses.order.set(2, lensNoOperators)
        lensNoOperators = InspectionItem.Lenses.id.set("New ID", lensNoOperators)
        lensNoOperators = InspectionItem.Lenses.id.set("1", lensNoOperators)
        lensNoOperators = InspectionItem.Lenses.type.set("Newest", lensNoOperators)
        lensNoOperators = InspectionItem.Lenses.order.set(10, lensNoOperators)


        var lensOperators = dummyItem
        // Lenses - with Operators

        lensOperators = lensOperators
            |> InspectionItem.Lenses.decorators .~ [0, 1, 2]
            |> InspectionItem.Lenses.type .~ "Additional"
        lensOperators = lensOperators
            |> InspectionItem.Lenses.type .~ "Mega type"
        /// Operators will help us, they are good and beatufiul don't blame smth yet that you can't read it's just a begining
        lensOperators =  lensOperators
            |> InspectionItem.Lenses.type .~ "Ordered"
            |> InspectionItem.Lenses.order .~ 2
        lensOperators = lensOperators
            |> InspectionItem.Lenses.id .~ "New ID"
        lensOperators = lensOperators
            |> InspectionItem.Lenses.id .~ "1"
            |> InspectionItem.Lenses.type .~ "Newest"
            |> InspectionItem.Lenses.order .~ 10


        // In One Cycle

        lensOperators = lensOperators
            |> InspectionItem.Lenses.decorators .~ [0, 1, 2] // decorators: 1
            |> InspectionItem.Lenses.type .~ "Additional"  // type: 4
            |> InspectionItem.Lenses.type .~ "Mega type"   // type: 3
            |> InspectionItem.Lenses.type .~ "Ordered"     // type: 2
            |> InspectionItem.Lenses.order .~ 2            // order: 2
            |> InspectionItem.Lenses.id .~ "New ID"        // id: 2
            |> InspectionItem.Lenses.id .~ "1"             // id: 1
            |> InspectionItem.Lenses.type .~ "Newest"      // type: 1
            |> InspectionItem.Lenses.order .~ 10           // order: 1

        // Remove duplications

        lensOperators = lensOperators
            |> InspectionItem.Lenses.decorators .~ [0, 1, 2]
            |> InspectionItem.Lenses.id .~ "1"
            |> InspectionItem.Lenses.type .~ "Newest"
            |> InspectionItem.Lenses.order .~ 10
    }
}
