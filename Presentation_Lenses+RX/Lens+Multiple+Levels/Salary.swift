struct Salary {
    var ammount: Ammount
    var bonus: Double
    var feet: Double
    var idm: Double
    var dcv: Double
    var weer: Double
    var sds: Double

    enum Lenses {
        static let ammount = Lens<Salary, Ammount>(
            get: { $0.ammount },
            set: { Salary(ammount: $0, bonus: $1.bonus, feet: $1.feet, idm: $1.idm, dcv: $1.dcv, weer: $1.weer, sds: $1.sds) }
        )

        static let bonus = Lens<Salary, Double>(
            get: { $0.bonus },
            set: { Salary(ammount: $1.ammount, bonus: $0, feet: $1.feet, idm: $1.idm, dcv: $1.dcv, weer: $1.weer, sds: $1.sds) }
        )
    }
}

struct Ammount {
    var value: Double
    var currency: String
    var id: String
    var date: String
    var state: String

    enum Lenses {
        static let value = Lens<Ammount, Double>(
            get: { $0.value },
            set: { Ammount(value: $0, currency: $1.currency , id: $1.id, date: $1.date, state: $1.state) }
        )

        static let currency = Lens<Ammount, String>(
            get: { $0.currency },
            set: { Ammount(value: $1.value, currency: $0 , id: $1.id, date: $1.date, state: $1.state) }
        )
    }
}

