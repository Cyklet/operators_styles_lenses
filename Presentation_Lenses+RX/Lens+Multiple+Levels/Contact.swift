struct Contact {
    var phone: String
    var mail: String
    var address: Address

    init(phone: String, mail: String, address: Address) {
        self.phone = phone
        self.mail = mail
        self.address = address
    }
}
