struct User {
    var fName: String
    var lName: String
    var contact: Contact
    var salary: Salary
	var isOld: Bool = false

    init(fName: String, lName: String, contact: Contact, salary: Salary) {
        self.fName = fName
        self.lName = lName
        self.contact = contact
        self.salary = salary
    }

    enum Lenses {
        static let salary = Lens<User, Salary>(
            get: { $0.salary },
            set: { User(fName: $1.fName, lName: $1.lName, contact: $1.contact, salary: $0) }
        )
        
    }
}

final private class UserExample {
	// MARK: Multiple Levels Nested Structs

		private var user = User(fName: "fname",
								lName: "lname",
								contact: Contact(phone: "phone",
												 mail: "mail",
												 address: Address(street: "street", city: "city")),
								salary: Salary(ammount: Ammount(value: 0, currency: "no", id: "id", date: "", state: ""),
											   bonus: 0, feet: 0, idm: 0, dcv: 0, weer: 0, sds: 0))

		private func mutltipleLevelsMutable() {
			let ammountValue: Double = 100
			var newAmmount = user.salary.ammount
			newAmmount.currency = "USD"
			newAmmount.value = ammountValue
			user.salary.ammount = newAmmount
			var newSalary = user.salary
			newSalary.bonus = ammountValue * 0.2
			user.salary = newSalary
			save(user: user)
		}

		private func mutlipleLevelsLens() {
			let ammountValue: Double = 100
			user = user
				|> User.Lenses.salary .. Salary.Lenses.ammount .. Ammount.Lenses.currency .~ "USD" // .\User.salary.ammount.currency
				|> User.Lenses.salary .. Salary.Lenses.ammount .. Ammount.Lenses.value .~ 100      // .\User.salary.ammount.value
				|> User.Lenses.salary .. Salary.Lenses.bonus .~ (ammountValue * 0.2)


			save(user: user)

	//        user = User.Lenses.salary.compose(Salary.Lenses.ammount.compose(Ammount.Lenses.currency)).set("USD", user)

	//        user = (User.Lenses.salary.compose(Salary.Lenses.ammount.compose(Ammount.Lenses.currency)) .~ "USD")(user)

	//        user = user
	//            |> (User.Lenses.salary.compose(Salary.Lenses.ammount.compose(Ammount.Lenses.currency)) .~ "USD")
	//            |> (User.Lenses.salary.compose(Salary.Lenses.ammount.compose(Ammount.Lenses.value)) .~ 100)
	//            |> (User.Lenses.salary.compose(Salary.Lenses.bonus) .~ (ammountValue * 0.2))

			save(user: user)
			user |> save
		}

		private func save(user: User) {
			// Save functionality
		}
}
