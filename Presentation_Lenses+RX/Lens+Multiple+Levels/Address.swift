struct Address {
    var street: String
    var city: String

    init(street: String, city: String) {
        self.street = street
        self.city = city
    }
}
