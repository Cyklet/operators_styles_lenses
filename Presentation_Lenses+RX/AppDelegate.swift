import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }

}


// 1. Good knowledge of functional programming
// 2. Higher order functions
// 3. Willingness to learn new things
// 4. Strong psychologically, hard to get scared
