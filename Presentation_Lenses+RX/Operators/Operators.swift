precedencegroup RightApplyPrecedence {
    associativity: left
    higherThan: AssignmentPrecedence
    lowerThan: TernaryPrecedence
}

precedencegroup LeftApplyPrecedence {
    associativity: left
    higherThan: RightApplyPrecedence
    lowerThan: TernaryPrecedence
}

precedencegroup ApplicativePrecedence {
    associativity: left
    higherThan: LogicalConjunctionPrecedence
    lowerThan: NilCoalescingPrecedence
}

precedencegroup FunctionCompositionPrecedence {
    associativity: right
    higherThan: ApplicativePrecedence
}

precedencegroup LensSetPrecedence {
    associativity: left
    higherThan: FunctionCompositionPrecedence
}

precedencegroup LensCompositionPrecedence {
    associativity: right
    higherThan: LensSetPrecedence
}

/// Lens set
infix operator .~: LensSetPrecedence

/// Pipe forward function application
infix operator |>: LeftApplyPrecedence

/// Forward composition
infix operator >>>: FunctionCompositionPrecedence

/// Backward composition
infix operator <<<: FunctionCompositionPrecedence

/// Lens composition
infix operator ..: LensCompositionPrecedence

/// Endomorphism composition
infix operator <>: FunctionCompositionPrecedence

/**
 Pipe a value into a function.

 - parameter x: A value.
 - parameter f: A function

 - returns: The value from apply `f` to `x`.
 */
func |> <A, B> (_ x: A, _ f: (A) -> B) -> B {
    return f(x)
}

/**
 Composes two functions in left-to-right order, i.e. (f >>> g)(x) = g(f(x)).

 - parameter f: A function.
 - parameter g: A function.

 - returns: A function that is the composition of `f` and `g`.
 */
func >>> <A, B, C> (_ f: @escaping (A) -> B, _ g: @escaping (B) -> C) -> (A) -> C {
    return { g(f($0)) }
}

/**
 Composes two functions in right-to-left order, i.e. (f <<< g)(x) = f(g(x)).

 - parameter f: A function.
 - parameter g: A function.

 - returns: A function that is the composition of `g` and `f`.
 */
func <<< <A, B, C> (_ f: @escaping (B) -> C, _ g: @escaping (A) -> B) -> (A) -> C {
    return { f(g($0)) }
}

/**
 Compose two endomorphisms in left-to-right order, i.e. (f <> g)(x) = g(f(x)) = x |> f |> g. Note that this
 operation is the monoid operation on the set of functions `A -> A`.

 - parameter f: A function.
 - parameter g: A function.

 - returns: A function that is the composition of `f` and `g`.
 */
func <> <A> (_ f: @escaping (A) -> A, _ g: @escaping (A) -> A) -> ((A) -> A) {
    return { g(f($0)) }
}


// Used in Lens

/**
 Infix operator of the `set` function.

 - parameter lens: A lens.
 - parameter part: A part.

 - returns: A function that transforms a whole into a new whole with a part replaced.
 */
func .~ <Whole, Part> (lens: Lens<Whole, Part>, part: Part) -> ((Whole) -> Whole) {
    return { whole in lens.set(part, whole) }
}

/**
 Infix operator of `compose`, which composes two lenses.

 - parameter lhs: A lens.
 - parameter rhs: A lens.

 - returns: The composed lens.
 */
func .. <A, B, C> (lhs: Lens<A, B>, rhs: Lens<B, C>) -> Lens<A, C> {
    return lhs.compose(rhs)
}


// Used in Styles

func <><A: AnyObject>(_ f: @escaping (A) -> Void, _ g: @escaping (A) -> Void) -> (A) -> Void {
	return { object in
		f(object)
		g(object)
	}
}
