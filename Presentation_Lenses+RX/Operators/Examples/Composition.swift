
extension Double {
	func add(_ value: Double) -> Double {
		return self + value
	}

	func mul(_ value: Double) -> Double {
	return self * value
	}

	func div(_ value: Double) -> Double {
		return self / value
	}
}
func withoutOperators() -> Double {
	var result: Double = 0
	result = result.add(10)
	result = result.mul(20)
	result = result.div(30)

	return result
}

func add(_ value: Double) -> (Double) -> Double {
	return { $0 + value }
}

func mul(_ value: Double) -> (Double) -> Double {
	return { $0 * value }
}

func div(_ value: Double) -> (Double) -> Double {
	return { $0 / value }
}

func withCustomOperators() -> Double {
	return 0 |> add(10) <> mul(20) <> div(30)
}

let defaultOperations = add(10) <> mul(20) <> div(30)

func perform(operations: (Double) -> Double, number: Double) -> Double {
	return number |> operations
}

// perform(operations: defaultOperations, number: 10)
// perform(operations: defaultOperations <> defaultOperations, number: 10)














